### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This project leverages on [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) to emulate deployment in a multi-cloud fashion.

The pipeline consists of two deployment stages;
- Review: GKE
- Staging: GKE
- Production: AWS ECS

Additional use cases include;
- Security scan; SAST, SCA, DAST, Secret Detection
- Security compliance pipeline
- Security policies


### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.
