var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'My Express' });
});


/* lookup vulnerability */
router.get('/lookup', (req,res) => {

  // https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules/-/blob/main/javascript/eval/rule-eval_with_expression.yml
  //user input file name contains the filename which will be given by the user
  
  var lookup = req.params.lookup;
  console.log("lookup: " + lookup)
  let toEval = eval(lookup); 
  res.send({"content":toEval});
  
  //let toEval = eval(req.query.input); 
  //res.send(toEval);

})


module.exports = router;
